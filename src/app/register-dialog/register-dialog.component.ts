import { ConditionalExpr } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators,FormControl,ReactiveFormsModule} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.css']
})
export class RegisterDialogComponent implements OnInit {

  // languages = new FormControl('');
  languagesList: string[] = ['English', 'Hindi', 'Gujarati', 'Bengali', 'Punjabi'];
  male = false;
  female = false;

  Users:any[] = []

  registerForm !: FormGroup

  constructor(private formBuilder:FormBuilder,private dialogRef:MatDialogRef<RegisterDialogComponent>) { }

  ngOnInit(): void {


    this.registerForm = this.formBuilder.group({
      salutation:['',Validators.required],
      firstName :['',Validators.required],
      lastName:['',Validators.required],
      email:['',[Validators.required,Validators.email]],
      mobileNumber:['',[Validators.required,Validators.minLength(10)]],
      department:['',Validators.required],
      gender:['',Validators.required],
      languages:['',Validators.required],
      salary:['',[Validators.required,Validators.pattern("^[0-9]*$")]],
      dob:[''],
      age:[''],

    })

  }

  get languages(): FormControl {
    return this.registerForm.get('languages') as FormControl;
  }


  get registerFormControl() {
    return this.registerForm.controls;
  }

  register(){
    if(this.registerForm.valid)
    {
      if(localStorage.getItem("users")){
        this.Users = JSON.parse(localStorage.getItem("users") as string)
        console.log(this.Users);
      }
     this.Users.push(this.registerForm.value);
     localStorage.setItem('users',JSON.stringify(this.Users));
      this.registerForm.reset();
      this.dialogRef.close("Save");
    }
  }

  getGender(e:any){

    if(this.registerForm.value["salutation"] == "Ms." || this.registerForm.value["salutation"] == "Mrs.") {
      this.female = true;
      this.registerForm.get("gender")?.setValue("Female");
    }
    else if(this.registerForm.value["salutation"] == "Mr."){
      this.male = true;
      this.registerForm.get("gender")?.setValue("Male");
    }
    else{
      this.female = false;
      this.male = false;
      this.registerForm.get("gender")?.setValue(null);
    }
    
  
  }

  calculateAge(e:any){
    if(e){
      let timeDiff = Math.abs(Date.now() - e.getTime());
      let age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
      this.registerForm.get("age")?.setValue(age);
    }
  }

  

  
}


