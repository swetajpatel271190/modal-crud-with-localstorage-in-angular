import { Component,ViewChild,AfterViewInit } from '@angular/core';
import { MatDialog,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RegisterDialogComponent } from './register-dialog/register-dialog.component';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';


export interface User {
  salutation:string,
  firstName: string;
  lastName: string;
  email: string;
  mobileNumber: string;
  department:string,
  gender:string,
  languages:string[],
  salary:number,
  dob:string,
  age:number


}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements AfterViewInit {
  displayedColumns: string[] = ['salutation', 'firstName', 'lastName', 'email','mobileNumber','department','gender','languages','salary','dob','age'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  Users!:any[]
  Test:User[]=[]
  constructor(public dialog: MatDialog){
   
    
    
    this.Users = JSON.parse(localStorage.getItem("users") as string)
    this.dataSource = new MatTableDataSource(this.Users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }



  openDialog() {
    this.dialog.open(RegisterDialogComponent, {
      width:'80%'
    }).afterClosed().subscribe(val=>{
      if(val=="Save"){
        this.getLocalStorage();
      }
    });
  }

  getLocalStorage(){
  
    localStorage.getItem("users");
    this.Users = JSON.parse(localStorage.getItem("users") as string)
    this.dataSource = new MatTableDataSource(this.Users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
  
 

